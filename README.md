# GIMP-Skript mit Aktionen zur Bearbeitung von Astrofotos

Dieses GIMP-Skript enthält eine Reihe von Aktionen, die zur Bearbeitung von Astrofotos nützlich sein können.

- [Sterne verkleinern](#sterne-verkleinern)
- [Sterne verkleinern (downscale)](#sterne-verkleinern-downscale)
- [DSOs aufhellen](#dsos-aufhellen)
- [Sterne auswählen](#sterne-auswählen)
- [Helle Sterne auswählen](#helle-sterne-auswählen)

Zu Download und Installation siehe [unten](#installation).

### Sterne verkleinern
Verkleinert Sterne, um das eigentliche Deep-Sky-Objekt mehr in den Vordergrund zu bringen.  
Das Ergebnis landet in einer neuen Ebene. Diese Aktion kann auch mehrfach angewendet werden, um den Effekt zu verstärken. Durch Verringern der Deckkraft der neuen Ebene kann der Effekt auch abgeschwächt werden.

![Vorher](examples/star-shrink-before.jpg)
![Nachher](examples/star-shrink-after.jpg)

### Sterne verkleinern (downscale)
Hat die gleiche Funktion wie **Sterne verkleinern**, skaliert aber anschließend das Bild herunter auf 50% Auflösung. 
Das kann nützlich sein, wenn z.B. mit 2x Drizzle gearbeitet wurde, um das Ergebnisbild wieder auf die 
ursprüngliche Auflösung zu reduzieren.

### DSOs aufhellen
Versucht, Deep-Sky-Objekte (DSOs) und schwache Sterne aufzuhellen, ohne dabei hellere Sterne noch heller
und größer werden zu lassen.  
Diese Aktion erzeugt eine neue Ebenen-Gruppe "DSOs aufhellen", die im Modus "Bildschirm" (auch "Screen" oder "negativ multiplizieren" genannt) über dem Originalbild liegt. In der Ebenen-Gruppe liegt eine sternlose Variante des Ursprungsbildes, sowie darüber eine Ebene mit dem Hintergrund, der von dem sternlosen Bild abgezogen wird. Damit enthält die Ebenen-Gruppe nur die DSO-Anteile, die dem Originalbild überlagert werden und dadurch stärker hervortreten.
Sowohl die sternlose Ebene, als auch das Hintergrund-Modell können noch von Hand weiter verfeinert werden. Soll nicht mehr weiter bearbeitet werden, dann kann das Ergebnis aber auch mit `Ebene -> Nach unten vereinen` zu einer Ebene zusammengefasst werden.

![Vorher](examples/lighten-dso-before.jpg)
![Nachher](examples/lighten-dso-after.jpg)

### Sterne auswählen
Erzeugt eine Auswahl, die möglichst nur Sterne enthält. Diese Aktion funktioniert gut, um Sterne auszuwählen, ohne dabei helle Deep-Sky-Objekte mit zu selektieren. Dafür werden sehr große, helle Sterne oft nicht mit ausgewählt.    
Die Auswahl liegt eng um die Sterne. Je nach Zweck der Auswahl ist es daher meist sinnvoll, die Auswahl noch etwas aufzuweiten (`Auswahl -> Vergrößern`) und mit einem weichen Übergang zu versehen (`Auswahl -> Ausblenden`).

### Helle Sterne auswählen
Ebenfalls ein Filter, der eine Auswahl für Sterne erstellt. Funktioniert besser für hellere Sterne und wählt schwache Sterne nicht mit aus.


## Installation
Das Skript [Astro-Tools_de.scm](https://gitlab.com/schreiberste/star-reduction-gimp-script-fu/-/raw/master/Astro-Tools_de.scm?inline=false) herunterladen und dabei darauf achten, dass die **Dateiendung `.scm`** erhalten bleibt. Firefox macht das richtig, aber andere Browser ändern die Endung gerne auf `.txt`. Im Zweifelsfall einfach die Datei umbenennen.

Dann muss das heruntergeladene Skript in den GIMP-Ordner für Plugin-Skripte gelegt werden.
Es gibt einen solchen Ordner pro Benutzer, sowie einen systemweiten Ordner, wenn die Astro-Tools für alle
Benutzer installiert werden sollen. Wo genau GIMP die Plugin-Skripte erwartet, findet man in Gimp unter
`Bearbeiten -> Einstellungen -> Ordner -> Skripte` heraus.

Unter Windows ist das normalerweise  
    `C:\Users\<Name>\AppData\Roaming\GIMP\2.10\scripts`   
Unter Linux   
    `/home/<Name>/.config/GIMP/2.10/scripts`

Nachdem das Skript heruntergeladen und in dem passenden Ordner abgelegt wurde, muss GIMP gestartet werden,
oder die Konfiguration mit `Filter -> Skript-Fu -> Skripte Auffrischen` neu geladen werden.

Dann sollten die Astro-Tools Aktionen im Menü unter `Filter -> Astro` auftauchen.
