(script-fu-register
  "script-fu-astro-shrink-stars"                              ;func name
  "Sterne verkleinern"                                        ;menu label
  "Verkleinert Sterne analog zur PS Action von Peter Shah. Erzeugt eine neue Ebene mit dem Ergebnis, die z.B. noch mit einer Sternmaske versehen werden kann, um andere Bereiche wie Nebel zu schützen."   ;description
  "Steffen Schreiber"                                         ;author
  ""                                                          ;copyright notice
  "2020"                                                      ;date created
  "*"                                                         ;image type that the script works on
  SF-IMAGE     "Image"         0                              ;parameter for input image
  SF-DRAWABLE  "Current Layer" 0                              ;parameter for drawable
)

(script-fu-menu-register "script-fu-astro-shrink-stars" "<Image>/Filters/Astro")

(define (script-fu-astro-shrink-stars img drawable)
  (let*
      ((owidth (car (gimp-image-width img)))
       (oheight (car (gimp-image-height img)))
      )
    
    ; init
    (gimp-context-push)
    (gimp-image-undo-group-start img)

    ; Arbeits-Layer-Kopie anlegen
    (define work-layer (car (gimp-layer-copy drawable 0)))
    (gimp-item-set-name work-layer "Sterne verkleinern")
    (gimp-image-insert-layer img work-layer 0 -1)

    ; Layer skalieren auf 200%
    (gimp-layer-scale work-layer (* 2 owidth) (* 2 oheight) 0)

    ; Erodieren, zu 50% anwenden 
    (plug-in-vpropagate 1 img work-layer 1 1 0.5 15 0 255)

    ; Skalieren zurück auf Originalgröße
    (gimp-layer-scale work-layer owidth oheight 0)

    ; Gaußscher Weichzeichner, Radius 0,5 Pixel, Ebenenmodus "Aufhellen"
    (define blur-layer (car (gimp-layer-copy work-layer 0)))
    (gimp-image-insert-layer img blur-layer 0 -1)
    (plug-in-gauss 1 img blur-layer 0.5 0.5 TRUE)
    (gimp-layer-set-mode blur-layer LAYER-MODE-LIGHTEN-ONLY)
    (set! work-layer (car (gimp-image-merge-down img blur-layer 1) ))

    ; Unscharf Maskieren (Stärke 80%, Radius 0,5 Pixel, Schwellenwert 0)
    (plug-in-unsharp-mask 1 img work-layer 0.5 0.8 0)
    
    ; Gaußscher Weichzeichner, Radius 0,3 Pixel, Ebenenmodus "Abdunkeln"
    (set! blur-layer (car (gimp-layer-copy work-layer 0)))
    (gimp-image-insert-layer img blur-layer 0 -1)
    (plug-in-gauss 1 img blur-layer 0.3 0.3 TRUE)
    (gimp-layer-set-mode blur-layer LAYER-MODE-DARKEN-ONLY)
    (set! work-layer (car (gimp-image-merge-down img blur-layer 1) ))

    ; tidy up
    (gimp-image-undo-group-end img)
    (gimp-displays-flush)
    (gimp-context-pop)
  )
)




(script-fu-register
  "script-fu-astro-shrink-stars-downscale"              ;func name
  "Sterne verkleinern (downscale)"                      ;menu label
  "Verkleinert Sterne analog zur PS Action von Peter Shah und skaliert anschließend das Bild auf 50% Größe herunter."   ;description
  "Steffen Schreiber"                                   ;author
  ""                                                    ;copyright notice
  "2020"                                                ;date created
  "*"                                                   ;image type that the script works on
  SF-IMAGE     "Image"         0                        ;parameter for input image
  SF-DRAWABLE  "Current Layer" 0                        ;parameter for drawable
)

(script-fu-menu-register "script-fu-astro-shrink-stars-downscale" "<Image>/Filters/Astro")

(define (script-fu-astro-shrink-stars-downscale img drawable)
  (let*
      ((owidth (car (gimp-image-width img)))
       (oheight (car (gimp-image-height img)))
      )
    
    ; init
    (gimp-context-push)
    (gimp-image-undo-group-start img)

    ; Arbeits-Layer-Kopie anlegen
    (define work-layer (car (gimp-layer-copy drawable 0)))
    (gimp-item-set-name work-layer "Sterne verkleinern")
    (gimp-image-insert-layer img work-layer 0 -1)

    ; Erodieren, zu 50% anwenden 
    (plug-in-vpropagate 1 img work-layer 1 1 0.5 15 0 255)

    ; Herunterskalieren auf halbe Originalgröße
    (gimp-image-scale img (* 0.5 owidth) (* 0.5 oheight))

    ; Gaußscher Weichzeichner, Radius 0,5 Pixel, Ebenenmodus "Aufhellen"
    (define blurred-layer (car (gimp-layer-copy work-layer 0)))
    (gimp-image-insert-layer img blurred-layer 0 -1)
    (plug-in-gauss 1 img blurred-layer 0.5 0.5 TRUE)
    (gimp-layer-set-mode blurred-layer LAYER-MODE-LIGHTEN-ONLY)
    (set! work-layer (car (gimp-image-merge-down img blurred-layer 1) ))

    ; Unscharf Maskieren (Stärke 80%, Radius 0,5 Pixel, Schwellenwert 0)
    (plug-in-unsharp-mask 1 img work-layer 0.5 0.8 0)
    
    ; Gaußscher Weichzeichner, Radius 0,3 Pixel, Ebenenmodus "Abdunkeln"
    (define blur2-layer (car (gimp-layer-copy work-layer 0)))
    (gimp-image-insert-layer img blur2-layer 0 -1)
    (plug-in-gauss 1 img blur2-layer 0.3 0.3 TRUE)
    (gimp-layer-set-mode blur2-layer LAYER-MODE-DARKEN-ONLY)
    (set! work-layer (car (gimp-image-merge-down img blur2-layer 1) ))

    ; tidy up
    (gimp-image-undo-group-end img)
    (gimp-displays-flush)
    (gimp-context-pop)
  )
)




(script-fu-register
  "script-fu-astro-select-stars"                        ;func name
  "Sterne auswählen"                                    ;menu label
  "Erstellt eine Auswahl für die Sterne."               ;description
  "Steffen Schreiber"                                   ;author
  ""                                                    ;copyright notice
  "2020"                                                ;date created
  "*"                                                   ;image type that the script works on
  SF-IMAGE     "Image"         0                        ;parameter for input image
  SF-DRAWABLE  "Current Layer" 0                        ;parameter for drawable
)

(script-fu-menu-register "script-fu-astro-select-stars" "<Image>/Filters/Astro")

(define (script-fu-astro-select-stars img drawable)
    ; init
    (gimp-context-push)
    (gimp-image-undo-group-start img)

    (if (not (= RGB (car (gimp-image-base-type img))))
       (gimp-image-convert-rgb img))	

    ; Arbeits-Layer-Kopie anlegen
    (define work-layer (car (gimp-layer-copy drawable 0)))
    (gimp-item-set-name work-layer "work")
    (gimp-image-insert-layer img work-layer 0 0)

    ; Entsättigen 
    (gimp-drawable-desaturate work-layer DESATURATE-LUMINANCE)

    ; High-Pass:
    ; blur mit Radius 1 (nur feine Sterne) - 10 (auch große Sterne)
    (define blurred-layer (car (gimp-layer-copy work-layer 0)))
    (gimp-item-set-name blurred-layer "blurred")
    (gimp-image-insert-layer img blurred-layer 0 -1)
    (plug-in-gauss 1 img blurred-layer 6.0 6.0 TRUE)

    (gimp-layer-set-mode blurred-layer GRAIN-EXTRACT-MODE)
    (set! work-layer (car (gimp-image-merge-down img blurred-layer 0)))
    (gimp-drawable-set-name work-layer "high pass")

    ; Gaußscher Weichzeichner, Radius 1 Pixel
    (plug-in-gauss 1 img work-layer 1.0 1.0 TRUE)

    ; Auswahl aus Bitmap mit Schwellwert 
    (gimp-drawable-threshold work-layer HISTOGRAM-VALUE 0.58 1)
    (gimp-image-select-color img CHANNEL-OP-REPLACE work-layer '(255 255 255))

    ; work-layer löschen
    (gimp-image-remove-layer img work-layer)
    (gimp-image-set-active-layer img drawable)

    ; tidy up
    (gimp-image-undo-group-end img)
    (gimp-displays-flush)
    (gimp-context-pop)
)




(script-fu-register
  "script-fu-astro-select-bright-stars"                 ;func name
  "Helle Sterne auswählen"                              ;menu label
  "Erstellt eine Auswahl für helle Sterne."             ;description
  "Steffen Schreiber"                                   ;author
  ""                                                    ;copyright notice
  "2020"                                                ;date created
  "*"                                                   ;image type that the script works on
  SF-IMAGE     "Image"         0                        ;parameter for input image
  SF-DRAWABLE  "Current Layer" 0                        ;parameter for drawable
)

(script-fu-menu-register "script-fu-astro-select-bright-stars" "<Image>/Filters/Astro")

(define (script-fu-astro-select-bright-stars img drawable)
    ; init
    (gimp-context-push)
    (gimp-image-undo-group-start img)

    (if (not (= RGB (car (gimp-image-base-type img))))
       (gimp-image-convert-rgb img))	

    (script-fu-astro-select-bright-stars-impl img drawable)

    ; tidy up
    (gimp-image-undo-group-end img)
    (gimp-displays-flush)
    (gimp-context-pop)
)



; Hilfsfunktion für die Erstellung einer Auswahl aus hellen Sternen
; in der gegebenen Ebene
(define (script-fu-astro-select-bright-stars-impl img drawable)
    ; Arbeits-Layer-Kopie anlegen
    (define work-layer (car (gimp-layer-copy drawable 0)))
    (gimp-item-set-name work-layer "Stern-Auswahl")
    (gimp-image-insert-layer img work-layer 0 0)

    ; Entsättigen 
    (gimp-drawable-desaturate work-layer DESATURATE-LUMINANCE)
 
    ; Farbton/Sättigung: Helligkeit -25
    (gimp-drawable-hue-saturation work-layer HUE-RANGE-ALL 0 -25 0 0)
    ; Gradationskurve
    (gimp-drawable-curves-spline work-layer HISTOGRAM-VALUE 14 #(0.0 0.0 0.258823529411765 0.043137254901961 0.537254901960784 0.231372549019608 0.87843137254902 0.545098039215686 0.933333333333333 0.631372549019608 0.968627450980392 0.76078431372549 0.984313725490196 0.870588235294118))
    ; Unscharf maskieren: Stärke 250%,  Radius 3px,  Schwellwert 15, Ebenenmodus "Aufhellen"
    (define tmp-layer (car (gimp-layer-copy work-layer 0)))
    (gimp-image-insert-layer img tmp-layer 0 0)
    (plug-in-unsharp-mask 1 img tmp-layer 3.0 250 15)
    (gimp-layer-set-mode tmp-layer LAYER-MODE-LIGHTEN-ONLY)
    (set! work-layer (car (gimp-image-merge-down img tmp-layer 1) ))
    
    ; Farbton/Sättigung: Helligkeit -25
    (gimp-drawable-hue-saturation work-layer HUE-RANGE-ALL 0 -25 0 0)
    ; Gradationskurve
    (gimp-drawable-curves-spline work-layer HISTOGRAM-VALUE 14 #(0.0 0.0 0.235294117647059 0.031372549019608 0.537254901960784 0.231372549019608 0.87843137254902 0.545098039215686 0.933333333333333 0.631372549019608 0.968627450980392 0.76078431372549 0.984313725490196 0.870588235294118))
    ; Unscharf maskieren: Stärke 500%,  Radius 1px,  Schwellwert 50, Ebenenmodus "Aufhellen"
    (set! tmp-layer (car (gimp-layer-copy work-layer 0)))
    (gimp-image-insert-layer img tmp-layer 0 0)
    (plug-in-unsharp-mask 1 img tmp-layer 1.0 300 50)
    (gimp-layer-set-mode tmp-layer LAYER-MODE-LIGHTEN-ONLY)
    (set! work-layer (car (gimp-image-merge-down img tmp-layer 1) ))
 
    ; Farbton/Sättigung: Helligkeit -25
    (gimp-drawable-hue-saturation work-layer HUE-RANGE-ALL 0 -25 0 0)
    ; Gradationskurve
    (gimp-drawable-curves-spline work-layer HISTOGRAM-VALUE 14 #(0.0 0.0 0.258823529411765 0.062745098039216 0.658823529411765 0.262745098039216 0.87843137254902 0.545098039215686 0.933333333333333 0.631372549019608 0.968627450980392 0.76078431372549 0.984313725490196 0.870588235294118))
    ; Gauß: Radius 1px
    (plug-in-gauss 1 img work-layer 1.0 1.0 TRUE)
    ; Tonwertkorrektur: Eingabe 0, 64  Gamma 0,74
    (gimp-drawable-levels work-layer HISTOGRAM-VALUE 0.0 0.25 FALSE 0.74 0.0 1.0 TRUE)
    ; Unscharf maskieren: Stärke 500%,  Radius 1px,  Schwellwert 50, Ebenenmodus "Aufhellen"
    (set! tmp-layer (car (gimp-layer-copy work-layer 0)))
    (gimp-image-insert-layer img tmp-layer 0 0)
    (plug-in-unsharp-mask 1 img tmp-layer 1.0 300 50)
    (gimp-layer-set-mode tmp-layer LAYER-MODE-LIGHTEN-ONLY)
    (set! work-layer (car (gimp-image-merge-down img tmp-layer 1) ))

    ; Gauß: Radius 1px, Modus aufhellen
    (set! tmp-layer (car (gimp-layer-copy work-layer 0)))
    (gimp-image-insert-layer img tmp-layer 0 0)
    (plug-in-gauss 1 img tmp-layer 1.0 1.0 TRUE)
    (gimp-layer-set-mode tmp-layer LAYER-MODE-LIGHTEN-ONLY)
    (set! work-layer (car (gimp-image-merge-down img tmp-layer 1) ))
 
    ; Gauß: Radius 0.5px
    (plug-in-gauss 1 img work-layer 0.5 0.5 TRUE)

    ; Layer in Selection umwandeln
    (define mask-channel (car (gimp-channel-new-from-component img CHANNEL-RED "mask")))
    (gimp-image-insert-channel img mask-channel 0 -1)
    (gimp-image-select-item img CHANNEL-OP-REPLACE mask-channel)
    (gimp-image-remove-channel img mask-channel)

    ; work-layer löschen
    (gimp-image-remove-layer img work-layer)
    (gimp-image-set-active-layer img drawable)
)



; Hilfsfunktion zum Erzeugen einer neuen Ebene mit einer entsternten
; Kopie der aktuellen Ebene.
(define (script-fu-astro-add-starless-layer-impl img input-layer)
    ; Auswahl aufheben
    (gimp-selection-none img)

    ; Arbeits-Layer-Kopie anlegen
    (define work-layer (car (gimp-layer-copy input-layer 0)))
    (gimp-item-set-name work-layer "Sternlos")
    (gimp-image-insert-layer img work-layer 0 -1)

    ; Sterne auswählen
    (script-fu-astro-select-bright-stars-impl img work-layer)

    ; Auswahl erweitern und mit weicher Kante versehen
    (gimp-selection-grow img 4)
    (gimp-selection-feather img 2.0)

    ; Sterne mit Median-Filter beseitigen
    ;    (radius,  typ (MEDIAN=0),  black-level thresh.,  white-level thresh.)
    (plug-in-despeckle 1 img work-layer 22 0 -1 220) 
    (plug-in-despeckle 1 img work-layer 18 0 -1 180)
    (plug-in-despeckle 1 img work-layer 16 0 -1 160)
    (plug-in-despeckle 1 img work-layer 14 0 -1 150)

    ; Auswahl aufheben
    (gimp-selection-none img)

    ; Ergebnislayer zurückgeben
    work-layer
)



; Hilfsfunktion zum Erzeugen eines künstlichen Hintergrundmodells.
; Funktioniert am besten mit einem sternlosen Bild als input-layer.
(define (script-fu-astro-add-background-model-layer-impl img input-layer)
    ; Arbeits-Layer-Kopie anlegen
    (define work-layer (car (gimp-layer-copy input-layer 0)))
    (gimp-item-set-name work-layer "Hintergrund-Modell")
    (gimp-image-insert-layer img work-layer 0 -1)

    ; Flecken entfernen: Radius=30, rekursiv (2),  Weiß-Schwellwert 40
    (plug-in-despeckle 1 img work-layer 30 2 -1 40)

    ; Weichzeichnen
    (plug-in-gauss 1 img work-layer 150.0 150.0 TRUE)

    ; Ergebnislayer zurückgeben
    work-layer
)



(script-fu-register
  "script-fu-astro-lighten-dso"                         ;func name
  "DSOs aufhellen"                                      ;menu label
  "Hebt die Helligkeit von Deep-Sky-Objekten (DSOs) und schwachen Sternen an."       ;description
  "Steffen Schreiber"                                   ;author
  ""                                                    ;copyright notice
  "2020"                                                ;date created
  "*"                                                   ;image type that the script works on
  SF-IMAGE     "Image"         0                        ;parameter for input image
  SF-DRAWABLE  "Current Layer" 0                        ;parameter for drawable
)

(script-fu-menu-register "script-fu-astro-lighten-dso" "<Image>/Filters/Astro")

(define (script-fu-astro-lighten-dso img drawable)
    ; init
    (gimp-context-push)
    (gimp-image-undo-group-start img)

    (if (not (= RGB (car (gimp-image-base-type img))))
       (gimp-image-convert-rgb img))	

    ; Sternlose Variante in neuem Layer anlegen
    (define starless-layer (script-fu-astro-add-starless-layer-impl img drawable))

    ; Neue Layer-Gruppe für die Sternlose Variante und deren abgezogenen Hintergrund
    (define dso-layer-group (car (gimp-layer-group-new img)))
    (gimp-item-set-name dso-layer-group "DSOs aufhellen")
    (gimp-image-insert-layer img dso-layer-group 0 -1)

    ; Ebenenmodus auf neg. multiplizieren
    (gimp-layer-set-mode dso-layer-group LAYER-MODE-SCREEN)

    ; Verschiebe sternlose Ebene in die DSO Layer-Gruppe
    (gimp-image-reorder-item img starless-layer dso-layer-group 0)
    
    ; Hintergrundmodell für sternlose Variante in neuem Layer anlegen
    (define bgmodel-layer (script-fu-astro-add-background-model-layer-impl img starless-layer))

    ; In die DSO Layer-Gruppe verschieben und auf Ebenenmodus DIFFERENCE,
    ; um den Hintergrund vom sternlosen Layer abzuziehen
    (gimp-image-reorder-item img bgmodel-layer dso-layer-group 0)
    (gimp-layer-set-mode bgmodel-layer LAYER-MODE-DIFFERENCE)

    ; DSO-Gruppe als neuen aktiven Layer wählen
    (gimp-image-set-active-layer img dso-layer-group)

    ; tidy up
    (gimp-image-undo-group-end img)
    (gimp-displays-flush)
    (gimp-context-pop)
)

